/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
// require("node-libs-react-native/globals");
import React from "react";
import { Component } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import Aquila from "./bgComponents/aquila/aquila";

import nodejs from "nodejs-mobile-react-native";
import init from "react_native_mqtt";
import { AsyncStorage } from "react-native";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      devices: new Set(),
      errColor: "green",
      uartTxText: "empty",
      uartTxCredits: "20",
      payload: "default",
      payload2: "pl2",
      moscaReady: false
    };

    this.aquila = null;
    this.appClient = null;
    this.apClient = null;
    this.aquGW = null;

    nodejs.start("main.js");
    nodejs.channel.addListener(
      "message",
      msg => {
        if (msg === "mosca_ready") {
          this.setState({moscaReady:true});
        }
        console.warn("From node: " + msg);
      },
    );
  }

  connectMqtt = async () => {
    init({
      size: 10000,
      storageBackend: AsyncStorage,
      defaultExpires: 1000 * 3600 * 24,
      enableCache: true,
      reconnect: true,
      sync: {}
    });

    await this.ensureNodeBackgroundIsRunning();

    this.aquila = new Aquila();

    this.appClient = new Paho.MQTT.Client(
      "localhost",
      Number(3000),
      "cliid123"
    );
    this.apClient = new Paho.MQTT.Client("test.mosquitto.org", 8080, "1asdasd");

    this.appClient.onConnectionLost = this.onClientOffline;
    this.appClient.onMessageArrived = this.onClientMessage;
    this.appClient.onClientConnect = this.onClientConnect;
    this.appClient.connect({
      onSuccess: this.onClientConnect,
      timeout: 1,
      reconnect: true
    });

    // this.apClient = new Paho.MQTT.Client("test.mosquitto.org", Number(8080), "1asdasd");
    this.apClient.onConnectionLost = this.onClientOffline;
    this.apClient.onMessageArrived = this.onClientMessage2;
    this.apClient.onClientConnect = this.onClientConnect2;
    this.apClient.connect({
      onSuccess: this.onClientConnect2,
      timeout: 10,
      reconnect: true
    });

    await this.ensureMqttClientIsConnected();
    await this.ensureMqttClient2IsConnected();

  };

  callFastMSG = () => {
    var fastmsg = new Paho.MQTT.Message("E6652D73B2F6D59AD08130483F0770F38794F69C4C3B1C4CC58E9DBB90EC6EFF4A7B7105A1BF8C762E82DC1B0B39750D37CABC069D1C4661C2914E3D11792082");
    fastmsg.destinationName = "mqtsn-xyz";
    this.apClient.send(fastmsg);
  };

  onClientConnect = () => {
    // Subscribe to all saved topics on connect or reconnect
    console.warn("Connected to MQTT broker", this.appClient.isConnected());

    this.appClient.subscribe("mqtsn-sub");
    this.appClient.subscribe("mqtsn-echo");
    // this.appClient.subscribe("World");
    let message = new Paho.MQTT.Message("Hello");
    message.destinationName = "World";
    this.appClient.send(message);
  };

  onClientConnect2 = () => {
    console.warn("App2Client");
    let callFastMSGint = setInterval(this.callFastMSG, 10);
    this.apClient.subscribe("mqtsn-sub2");
  };

  onClientOffline = () => {
    console.warn("MQTT broker offline");
  };

  onClientReconnect = () => {
    console.warn("Trying to reconnect with MQTT broker");
  };

  onClientMessage = async message => {
    // console.warn("Client Message recieved:", message.payloadString);
    this.setState({ payload: message.payloadString });
  };

  onClientMessage2 = async message => {
    // console.warn("Client Message recieved:", message.payloadString);
    this.setState({ payload2: message.payloadString });
  };

  ensureMqttClientIsConnected = () => {
    return new Promise((resolve, reject) => {
      this.waitForMqttClientIsConnected(resolve);
    });
  };

  ensureMqttClient2IsConnected = () => {
    return new Promise((resolve, reject) => {
      this.waitForMqttClient2IsConnected(resolve);
    });
  };

  waitForMqttClientIsConnected = resolve => {
    if (!this.appClient.isConnected()) {
      setTimeout(this.waitForMqttClientIsConnected.bind(this, resolve), 200);
    } else {
      resolve();
    }
  };

  waitForMqttClient2IsConnected = resolve => {
    if (!this.apClient.isConnected()) {
      setTimeout(this.waitForMqttClient2IsConnected.bind(this, resolve), 200);
    } else {
      resolve();
    }
  };


  ensureNodeBackgroundIsRunning = () => {
    return new Promise((resolve, reject) => {
      this.waitNodeBackgroundIsRunning(resolve);
    });
  };

  waitNodeBackgroundIsRunning = resolve => {
    if (!this.state.moscaReady) {
      nodejs.channel.send('are_you_ready');
      setTimeout(this.waitNodeBackgroundIsRunning.bind(this, resolve), 200);
    } else {
      resolve();
    }
  };

  componentWillMount() {
    this.connectMqtt();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native</Text>
        <View style={styles.containerHoriz}>
          {/* <FlatList
            style={styles.list}
            data={Array.from(this.state.devices)}
            renderItem={({ item }) => (
              <DeviceRender
                device={item}
                blemanager={this.manager}
                uartTxTextCB={this.deviceRenderCallbackTxData}
                uartTxCreditsCB={this.deviceRenderCallbackTxCredits}
              />
            )}
            keyExtractor={({ id }) => id}
          /> */}
          <View
            style={{ width: "30%", height: "100%", backgroundColor: "#DFDFFF" }}
          >
            <Text>{this.state.uartTxText}</Text>
            <Text>{this.state.uartTxCredits}</Text>
          </View>
        </View>
        <View
          style={[styles.status, { backgroundColor: this.state.errColor }]}
        />

        <Text
          style={{
            fontSize: 80,
            fontWeight: "bold"
          }}
        >
          {this.state.payload}
        </Text>

        <Text
          style={{
            fontSize: 80,
            fontWeight: "bold",
            fontColor: "blue"
          }}
        >
          {this.state.payload2}
        </Text>

        {/* <Button
          style={styles.button}
          onPress={this.deleteDevices}
          title="Delete Devices"
          color="#841584"
        /> */}
        <Button
          title="Message Node"
          onPress={() => {
            let message = new Paho.MQTT.Message("subscribe");
            message.destinationName = "mqtsn-sub";
            this.appClient.send(message);
          }}
        />

        <Button
          title="Message IOT"
          onPress={() => {
            let message = new Paho.MQTT.Message("apClient");
            message.destinationName = "mqtsn-xyz";
            this.apClient.send(message);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    margin: 20
  },
  list: {
    margin: 10,
    width: "50%"
  },
  status: { height: 20, width: 20 },
  button: {
    marginBottom: 100
  },
  containerHoriz: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  }
});
