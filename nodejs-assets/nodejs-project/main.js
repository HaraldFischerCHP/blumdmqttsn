// Rename this sample file to main.js to use on your project.
// The main.js file will be overwritten in updates/reinstalls.

var rn_bridge = require("rn-bridge");
var mosca = require("mosca");
var mqtt = require("mqtt");
var mqclient = mqtt.connect("mqtt://test.mosquitto.org");


var im_ready = false;
var _mqclient_publish = null;
var _mqclient_subscribe = null;



mqclient.on("connect", function() {
  // mqclient.subscribe('world');
  _mqclient_publish = (topic, payload) => {
    mqclient.publish(topic, payload);
  };
  _mqclient_subscribe = topic => {
    mqclient.subscribe(topic);
  };
  // rn_bridge.channel.send("clientconnectedto" + mqclient);
  // rn_bridge.channel.send("pubfun" + _mqclient_publish);
});

// var mosca = require("mosca");
var server = new mosca.Server({
  http: {
    port: 3000,
    bundle: true,
    static: "./"
  }
});

server.on("clientConnected", function(client) {
  rn_bridge.channel.send("client connected", client.id);
});

// fired when a message is received
server.on("published", function(packet, client) {
  // rn_bridge.channel.send(
  //   "Published" + "_t_" + packet.topic + "_p_" + packet.payload + "_c_" + client
  // );
  if (_mqclient_publish !== null) {
    if (client !== undefined) {
      _mqclient_publish(packet.topic, packet.payload);
    }
  }
});

// fired when a message is received
server.on("subscribed", function(topic, client) {
  // rn_bridge.channel.send("subcribed" + "_t_" + topic + "_c_" + client);
  if (_mqclient_subscribe !== null) {
    _mqclient_subscribe(topic);
  }
});

mqclient.on("message", function(topic, message, packet) {
  // message is Buffer
  // rn_bridge.channel.send("message" + "_t_" + topic + "_c_" + JSON.stringify(packet));
  server.publish(packet);
});

// // fired when a message is received
// server.on("subscribed", function(topic, client) {
//   rn_bridge.channel.send("subscribed", topic);
//   client.subscribe(topic);
// });

// client.on("packetreceive", function(packetreceive) {
//   server.publish(packetreceive);
// });

server.on("ready", setup);

// fired when the mqtt server is ready
function setup() {
  im_ready = true;
  rn_bridge.channel.send("mosca_ready");
}

// Echo every message received from react-native.
rn_bridge.channel.on("message", msg => {
  if (msg === "are_you_ready") {
    if (im_ready) {
      rn_bridge.channel.send("mosca_ready");
    }
  }
});