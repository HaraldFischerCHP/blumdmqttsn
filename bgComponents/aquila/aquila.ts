import { EventEmitter } from "events";
import { TerminalIOTransport } from "./TerminalIOTransport";
import { GatewayDB } from "./GatewayDB";
import { Forwarder } from "./Forwarder";
import { Gateway } from "./Gateway";
import { GwMonitor } from "./GwMonitor";
import { TransportInterface, DBInterface } from "./interfaces";

export default class Aquila extends EventEmitter {
  db: DBInterface;
  forwarder: Forwarder;
  transport: TransportInterface;
  client: mqtt.Client = null;
  externalClient: boolean = false;
  allowUnknownDevices: boolean = true;
  keepAliveInterval: NodeJS.Timer = null;
  advertiseInterval: NodeJS.Timer = null;

  _onClientConnect: any;
  _onClientOffline: any;
  _onClientReconnect: any;
  _onClientMessage: any;
  _onParserError: any;

  constructor(externClient:mqtt.Client) {
    super();

    let db = new GatewayDB();
    let gw: Gateway;
    let transport = new TerminalIOTransport("SNR");
    let client = externClient;
    // transport.on("status", (data: string) => {
    //   this.emit("status", this.showMessage1);
    // });

    // let IntervallshowMessage = setInterval(this.showMessage, 2000);

    db.connect()
      .then(() => {
        let forwarder = new Forwarder(db, transport, null, null);
        gw = new Gateway(db, forwarder);
        return gw.init("test.mosquitto.org", true);
      })
      .then(() => {
        console.warn("gw init");
        let gwMon = new GwMonitor(gw);
        console.log("Gateway Started");
      })
      .catch( (error:any) => {
        // Handle errors
        console.log("connect error:", error);
      });

    // this.emit("test", "constructed aquila");
  }

  // showMessage = () => {
  //   this.emit("test", "timed emit test");
  // };

  // showMessage1 = (data: string) => {
  //   this.emit("test", data);
  // };
}
