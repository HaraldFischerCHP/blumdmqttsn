// SerialTransport.js
import { EventEmitter } from "events";
import * as Slip from "node-slip";
import { calcCrc, checkCrc } from "./CrcUtils";
import { TransportInterface } from "./interfaces";

import {
  BleManager,
  fullUUID,
  Device,
  Characteristic,
  Service,
  BleError
} from "react-native-ble-plx";
// import { Settings } from "react-native";

const TIO_SERVICE_UUID = "FEFB";
const TIO_UART_RX_UUID = "00000001-0000-1000-8000-008025000000";
const TIO_UART_TX_UUID = "00000002-0000-1000-8000-008025000000";
const TIO_UART_RX_CREDITS_UUID = "00000003-0000-1000-8000-008025000000";
const TIO_UART_TX_CREDITS_UUID = "00000004-0000-1000-8000-008025000000";

const TIOUUIDS = [
  TIO_SERVICE_UUID,
  TIO_UART_RX_UUID,
  TIO_UART_TX_UUID,
  TIO_UART_RX_CREDITS_UUID,
  TIO_UART_TX_CREDITS_UUID
];

class TerminalIOConnector extends EventEmitter {
  blemanager: BleManager;
  devices: Array<Device>;
  uartCredits: Number;
  tioUartRxDataChara: Characteristic;
  tioUartRxCreditsChara: Characteristic;

  constructor() {
    super();
    this.uartCredits = 0;
    this.blemanager = new BleManager();
    this.devices = [];
    this.blemanager.stopDeviceScan();
    const subscription = this.blemanager.onStateChange(state => {
      if (state === "PoweredOn") {
        this.scanAndConnect();
        subscription.remove();
      }
    }, true);
  }

  checkBLEStatus = () => {
    if (this.blemanager) {
      this.blemanager
        .state()
        .then(data => {
          console.log("checkBLEStatus", data);
        })
        .catch(() => {
          // console.warn("catch");
        });
    }
    for (let it of this.devices) console.log("found devices", it.name);
    // this.scanAndConnect();
  };

  bleScanListCallback = (error?: BleError, device?: Device) => {
    if (error) {
      console.debug("err: ", error);
      this.scanAndConnect();
      return;
    }
    var addThis = true;
    for (let item of this.devices) {
      if (device.id === item.id) {
        addThis = false;
      }
    }
    if (addThis) {
      this.devices.push(device);
    }

    this.checkAllServices(device);
  };

  scanAndConnect = () => {
    this.blemanager.startDeviceScan(TIOUUIDS, null, this.bleScanListCallback);
  };

  deleteDevices = () => {
    this.blemanager.stopDeviceScan();
    const subscription = this.blemanager.onStateChange(state => {
      if (state === "PoweredOn") {
        this.scanAndConnect();
        subscription.remove();
      }
    }, true);
  };

  checkAllServices = (device: Device) => {
    if (device === null) {
      return;
    }

    device
      .connect()
      .then((device: any) => {
        device.onDisconnected((error: BleError, device: Device) => {
          this.close();
        });
        return device;
      })
      .then((device: any) => {
        // console.log("connectedTo: ", device.id);
        var dev = device.discoverAllServicesAndCharacteristics();
        // console.log("device connected: ", dev.id);
        return dev;
      })
      .then((device: any) => {
        var services = device.services();
        // console.log("services: ", services);
        return device;
      })
      .then((device: Device) => {
        console.log("services: ", device.services());
        return device.characteristicsForService(fullUUID(TIO_SERVICE_UUID));
      })
      .then((chara: any) => {
        // console.log(chara);
        chara.forEach((element: Characteristic) => {
          if (element.uuid === TIO_UART_TX_CREDITS_UUID) {
            const txcreditsubscription = element.monitor(
              (error?: BleError, characteristic?: Characteristic) => {
                this.uartCreditsTxCallback(characteristic, error);
              }
            );
          } else if (element.uuid === TIO_UART_TX_UUID) {
            const txdatasubscription = element.monitor(
              (error?: BleError, characteristic?: Characteristic) => {
                this.uartDataTxCallback(characteristic, error);
              }
            );
          } else if (element.uuid === TIO_UART_RX_CREDITS_UUID) {
            this.tioUartRxCreditsChara = element;
          } else if (element.uuid === TIO_UART_RX_UUID) {
            this.tioUartRxDataChara = element;
          }
        });
      })
      .then(() => {
        this.uartCreditsRxCallback(20);
      })
      .catch((error: BleError) => {
        // Handle errors
        console.log("connect error:", error);
      });
  };

  close = () => {
    this.blemanager.stopDeviceScan();
    const subscription = this.blemanager.onStateChange(state => {
      if (state === "PoweredOn") {
        this.scanAndConnect();
        subscription.remove();
      }
    }, true);
    this.devices = [];
  };

  uartCreditsTxCallback = (
    characteristic?: Characteristic,
    error?: BleError
  ) => {
    if (characteristic) {
      var buf = Buffer.from(characteristic.value, "base64");
      if (buf.length == 0) return;
      var value = buf.map(String)[0];
      // console.log(value);
      this.txCreditsCB(value);
    }
  };

  uartDataTxCallback = (characteristic?: Characteristic, error?: BleError) => {
    if (characteristic) {
      var value = Buffer.from(characteristic.value, "base64");
      this.emit("data", value);
      console.log("TIO recv: ", value);
      // this.txCallback(value);
      this.uartCreditsRxCallback(1);
    }
  };

  uartDataRxCallback = (rxData: any) => {
    if (this.tioUartRxDataChara) {
      if (rxData) {
        this.uartCredits--;
        var valueBase64 = Buffer.from(rxData, "ascii").toString("base64");
        this.tioUartRxDataChara.writeWithoutResponse(valueBase64);
      }
    }
  };

  uartCreditsRxCallback = (creditsCount: number) => {
    if (this.tioUartRxCreditsChara) {
      var value = [creditsCount];
      var valueBase64 = Buffer.from(value).toString("base64"); // sending free credits to transmit - this is a kind of handshake
      this.tioUartRxCreditsChara.writeWithResponse(valueBase64);
    }
  };

  txCreditsCB = value => {
    this.uartCredits = value;
  };

  txBusy = () => {
    return this.uartCredits <= 0;
  };

  checkTXCredits = () => {
    console.log("credits", this.uartCredits);
  };
}

export class TerminalIOTransport extends EventEmitter
  implements TransportInterface {
  fake: boolean = false;
  // Serial port write buffer control
  writing: boolean = false;
  writeBuffer: Array<any> = [];
  bleconnector: TerminalIOConnector;

  parser: any;

  constructor(id: string) {
    super();
    // this.emit("test", "TIO transporter constructed");

    const receiver = {
      data: (input: Buffer) => {
        // Check CRC
        let crcOk = checkCrc(input);
        // Strip CRC data
        let data = input.slice(0, input.length - 2);

        if (crcOk) {
          this.emit("data", data);
        } else {
          this.emit("crcError", data);
        }
      },
      framing: (input: Buffer) => {
        this.emit("framingError", input);
      },
      escape: (input: Buffer) => {
        this.emit("escapeError", input);
      }
    };

    this.parser = new Slip.parser(receiver);

    this.bleconnector = new TerminalIOConnector();

    this.bleconnector.scanAndConnect();
    // this.bleconnector.checkAllServices();

    let IntervallshowMessage = setInterval(
      this.bleconnector.checkBLEStatus,
      2000
    );
    let IntervallcheckCredits = setInterval(
      this.bleconnector.checkTXCredits,
      2000
    );

    this.bleconnector.on("data", data => {
      this.parser.write(data);
    });

    this.bleconnector.on("open", () => {
      this.emit("ready");
    });

    this.bleconnector.on("error", err => {
      this.emit("error", err);
    });

    this.bleconnector.on("disconnect", err => {
      this.emit("disconnect", err);
    });

    this.bleconnector.on("close", () => {
      this.emit("close");
    });
  }

  checkStatus = () => {
    if (this.bleconnector) return this.bleconnector.checkBLEStatus();
  };

  connect(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      // this.uart.open((err: any) => {
      //   if(err) {
      //     this.emit("error", err);
      //     return reject(err);
      //   }
      //   return resolve(null);
      // });
      return resolve(null);
    });
  }

  // close(callback: Function) {
  //   if(!callback) callback = function(){};
  //   this.uart.flush((err) => {
  //     if(err) return callback(err);
  //     this.uart.drain((err) => {
  //       if(err) return callback(err);
  //       this.uart.close( () => callback() );
  //     });
  //   });
  // }

  write(data: any) {
    data = new Buffer(data);
    // Append CRC
    let crc = calcCrc(data);
    let crcBuf = new Buffer(2);

    crcBuf.writeUInt16LE(crc, 0);

    let buffer = Buffer.concat([data, crcBuf]);

    // Convert to Slip
    let slipData = Slip.generator(buffer);

    this.writeBuffer.push(slipData);
    this.writeNow();
  }

  close(callback: Function) {
    if (this.bleconnector) this.bleconnector.close();
  }

  writeNow() {
    // Nothing to do here
    if (this.writeBuffer.length <= 0) return;
    // We are busy, do nothing
    // if(this.writing) return;

    // this.writing = true;
    if (!this.bleconnector.txBusy()) {
      let data = this.writeBuffer.shift();
      this.bleconnector.uartDataRxCallback(data);
      //if(config.debug) console.log("Sending:", data);
      // this.writing = false;
      if (this.writeBuffer.length > 0) this.writeNow();
    }
  }
}
